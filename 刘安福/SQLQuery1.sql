create database Invoicing
use Invoicing

--基本资料
		--客户信息          
create table ClientInfor(
	ClientId int primary key identity (1,1) ,		--客户编号
	ClientName varchar(20) not null	,	--客户姓名
	ClientPhone varchar(30) not null ,		--客户电话
	ClientAddress varchar(100) not null		--客户地址
						)
		--供应商信息
create table Supplier(
	SupplierId int primary key identity (1,1) ,			--供应商编号
	SupplierName varchar(40) not null ,		--供应商名称
	SupplierPhone varchar(20) not null ,		--手机号码
	RawMaterial varchar(20) not null ,		--提供原材料
	Equipment varchar(30) not null	,	--设备
	Energy varchar(50)		--能源
					)
--销售管理
		--报价单
create table Quotation(
	CommodityId int primary key identity (1,1), 		--商品编号
	CommodityName varchar(30) not null,		--商品名称 
	CommodityRetailPrice money not null,		--商品零售价格
	CommodityWholesalePrice money not null,		--批发价格
					)
		--销售订单
create table SalesOrder(
	CommodityId int primary key not null ,		--销售商品编号
	COmmodityName varchar(30) not null ,		--销售商品名称 
	SalesAmount int not null default(0) , 		--商品销售数量
	OrderCreateTime smalldatetime default(getdate())		--订单生成时间
						)
		--销售出库
create table SalesOfOutbound(
	CommodityId int primary key not null ,		--出库商品编号
	OutboundAmount int not null default(0) ,		--商品出库数量
	OutboundTime smalldatetime not null default(getdate())		--出库时间
							)
		--销售退货
create table SalesReturn(
	CommodityId int primary key not null ,		--退货商品编号
	ReturnAmount int not null default(0) ,		--退货数量
	ReturnWhy varchar(200) default(' ')	,	--退货原因 varchar(200)
	ReturnTime smalldatetime default(getdate())		--退货时间
						)
--采购管理
		--采购计划/请购
create table PurchaseRequest(
	RequestId int primary key identity(1,1),		--请购商品编号
	RequestName varchar(30) not null ,		--请购商品名称
	RequestAmount int not null default(0),		--请购数量
	RequestType varchar(20) not null ,		--商品型号 
	RequestSpecification varchar(20) not null ,		--商品技术参数 
	RequestMaterial varchar(20) not null ,		--商品材质
	RequestTime smalldatetime not null default(getdate())		--请购时间
							)
		--采购订单
create table PurchaseOrder(
	RequestId int primary key not null ,		--请购商品编号
	RequestAmount int not null default(0) ,		--采购数量
	OrderTime smalldatetime not null default(getdate())		--采购时间
							)
		--验收入库
create table AcceptanceIntoThe(
	CommodityId int primary key not null ,		--商品编号
	CommodityName varchar(30) not null ,		--入库商品名称
	IntoTheAmount int not null default(0) ,		--入库数量
	IntoTheTime smalldatetime not null default(getdate())		--入库时间
							)
		--采购退料
create table StoresReturned(
	RequestId int primary key not null ,		--退料商品编号
	RequestName varchar(30) not null ,		--退料商品名称 
	ReturnedAmount int not null default(0) ,		--退料商品数量
	ReturnedTime smalldatetime not null default(getdate())		--退料时间
							)

--库存管理
		--库存调拨(两个仓库之间的货物相互调配)
create table InventoryAllocation(
	CommodityId int primary key not null ,		--调拨商品编号
	CommodityName varchar(30) not null ,		--调拨商品名称
	AllocationAmount int not null default(0) ,		--调拨数量
	AllocationTime smalldatetime not null default(getdate())		--调拨时间
								)
		--成本计划
create table CostPlanning(
	CommodityId int primary key not null ,		--商品编号
	CommodityName varchar(30) not null ,		--商品名称
	Budget money not null default(0.00) ,		--预计花费
	ActualCost money not null default(0.00)		--实际花费
						)
		--库存
create table Inventory (
	CommodityId int primary key not null ,		--商品编号
	CommodityName varchar(30) not null ,		--商品名称
	CommodityInventory int not null default(0) , 		--商品库存
	InventoryAmount int not null default(0) ,		--总库存
	InventoryIsNow int not null default(0)		--现库存
						)

--生产管理 
		--生产工单
create table  WorkOrder( 
	CommodityId int primary key not null ,		--生产商品编号
	CommodityName varchar(30) not null ,		--生产商品名称
	ProductionAmount int not null default(0) ,		--生产数量
	ProductionTime smalldatetime not null default(getdate())		--生产时间
						)
		--生产缺料、退料
create table ProductionStarvingAndReturned(
	CommodityId int primary key not null , 		--生产商品编号
	CommodityName varchar(30) not null ,		--生产商品名称
	StarvingAmount int not null default(0) ,		--缺料数量
	ReturnedAmount int not null default(0) ,		--退料数量
	ReturnedTime smalldatetime not null default(getdate()) -- 退料时间
										)
		--生产入库
create table YDGoodsYield(
	CommodityId int primary key not null , 		--入库商品编号
	CommodityName varchar(30) not null ,		--入库商品名称
	IntoCommodityAmount int not null default(0) ,		--入库商品数量
	IntoTime smalldatetime not null default(getdate())	--入库时间
						)
		--生产退回
create table ProductionReturn(
	CommodityId int primary key not null ,		--退回商品编号
	CommodityName varchar(30) not null ,		--退回商品名称
	ReturnAmount int not null default(0) ,		--退回数量
	ReturnTime smalldatetime not null default(getdate())			--退回时间
							)
--应收应付
		--应收账款
create table Receivables(		
	ReceivablesId int primary key identity(1,1) ,		--收款编号
	ReceivablesProceeds money not null default(0.00) ,		--销售收款
	ReceivablesTime smalldatetime not null default(getdate())		--收款时间
						)
		--应付账款
create table AccountsPayable(
	PayId int primary key identity(1,1) , 		--付款编号
	AccountsPayable money not null default(0.00) ,		--采购应付
	PayTime smalldatetime not null default(getdate())		--付款时间
							)